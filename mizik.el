;;; mizik.el --- Mizik                   -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Nzgg

;; Author: Nzgg <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Nzgg/emacs-mizik
;; Package-Requires: ((emacs "29.1") (libmpdee "2.2"))
;; Version: 1.0
;; Keywords: multimedia, music, mpd

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; A client for the Music Player Daemon, inspired by ncmpcpp and Mingus.

;;; Code:
(require 'libmpdee)
;; cl-case
;; cl-first
;; cl-third
;; cl-fifth
;; cl-second
;; cl-fourth
;; cl-reduce
;; cl-find-if
;; cl-position
;; cl-remove-if-not
(require 'cl-lib)
;; font-lock--remove-face-from-text-property
(require 'font-lock)
;; kill-whole-line
;; transpose-lines
(require 'simple)
;; sort-subr
(require 'sort)
;; format-seconds
(require 'time-date)

(defconst mizik---buffer-name-library "♫ Mizik ♩"
  "The name of the buffer the Mizik MPD client's library always runs in.")
(defconst mizik---buffer-name-playlist-format "♭ {} ♬"
  "The format for the name of a buffer for a playlist within Mizik.")
(defconst mizik---buffer-name-queue "♪ Queue ♯"
  "The name of the buffer the Mizik MPD client's queue always runs in.")

(defconst mizik---filter-tags-all '("file"  "artist"       "album"
                                    "title" "track"        "genre"
                                    "disc"  "album-artist" "time")
  "A list of all common tags usually provided by MPD for song resources.

Only really used to automate the creation of `mizik---filter-keywords-all',
`mizik---filter-keywords-negate', `mizik---filter-keywords-regexp', and
`mizik---filter-keywords-regexp-negate'.")
(defconst mizik---filter-keywords-all `(,@(mapcar (apply-partially #'concat "@")
                                                  mizik---filter-tags-all)
                                        "$or"
                                        "#")
  "Autocomplete keywords when filling in a filter-string in the minibuffer.")
(defconst mizik---filter-keywords-negate (mapcar (apply-partially #'concat "@!")
                                                 mizik---filter-tags-all)
  "Autocomplete tag-negating keywords when filling in a filter-string.")
(defconst mizik---filter-keywords-regexp (mapcar (apply-partially #'concat "@=")
                                                 mizik---filter-tags-all)
  "Autocomplete tag-by-regexp keywords when filling in a filter-string.")
(defconst mizik---filter-keywords-regexp-negate (mapcar (apply-partially #'concat "@~")
                                                        mizik---filter-tags-all)
  "Autocomplete tag-negating-by-regexp keywords when filling in a filter-string.")

(defvar mizik---currently-playing-buffer nil
  "Which Mizik buffer is currently utilizing the MPD queue.")

(defvar mizik---buffer-filters (make-hash-table :test 'equal)
  "A hashtable to store how a buffer's been filtered.")

(defvar mizik---filter-actively-edited nil
  "A variable to store active modifications to a buffer's filter string.

This is needed because, during live/active editing, the modifications made may
not be kept permanently, in the end (if, for example, the user uses
`keyboard-quit' while editing, rather than pressing Return).

Storing the changes, as they happen, here allows Mizik to understand how to
update the buffer while changes are made with the ability to revert back to
the original filter stored in `mizik---buffer-filters', if need be.

This variable can, also, function as a means to tell whether an live/active
session of modifying a buffer's filter is currently ongoing; if it's
\\='nil\\=', assume that the minibuffer is not, currently, in active use.")

(defvar mizik---mode-line-status nil
  "The string to show, in the modeline, of the current state of Mizik.")

(defvar mizik---mode-line-marquee-scroll-forward-p nil
  "Tells what direction the marquee text, in the modeline, is currently scrolling.")

(defvar mizik---mode-line-marquee-visible-text ""
  "The portion of the marquee text, in the modeline, that is currently visible.")

(defvar mizik---mode-line-timer nil
  "The timer which updates `mizik---mode-line-status' with the state of Mizik.")

(defcustom mizik-mode-line-timer-interval 1
  "How frequently the timer which updates `mizik---mode-line-status' will run.")

(defcustom mizik-mode-line-unicode-p t
  "Tell Mizik whether to use unicode or just ASCII characters in the modeline.

When using unicode characters, state in the modeline will be represented by the
characters in the following line with the corresponding ASCII characters used,
instead, otherwise:

▶ || ■ → ⟲ ⟲¹ ⟲⁰ ⇉ ⤭
P p  S   R R1 R0   s

The option is given because, while unique and familiar symbols often make it
easier for humans to ingest complex information quickly, not all fonts have
support for these characters; if you find you're unable to read some of the
characters in the first row, you may want to set this variable to \\='nil\\='."
  :type  'boolean
  :group 'mizik)

(defcustom mizik-mode-line-marquee-length 15
  "The length that `mizik---mode-line-marquee-visible-text' should be."
  :type  'integer
  :group 'mizik)

(defcustom mizik-mode-line-marquee-format "%ti by %ar"
  "Format for modeline text that mimics a scrolling, electronic marquee sign."
  :type  'string
  :group 'mizik)

(defcustom mizik-mode-line-static-preceding-format "⇤ %st ⇥ %re%si %sh "
  "Format for modeline text which is static and doesn't move, before the marquee."
  :type  'string
  :group 'mizik)

(defcustom mizik-mode-line-static-succeeding-format " [%te/%tt]"
  "Format for modeline text which is static and doesn't move, after the marquee."
  :type  'string
  :group 'mizik)

(defcustom mizik-completing-read-function #'completing-read
  "Requires the same arguments as `completing-read'."
  :type  'function
  :group 'mizik)

(defcustom mizik-songs-format-string "%ti34%ar33%al33"
  "The format used for displaying songs in the buffer."
  :type  'string
  :group 'mizik)

(defcustom mizik-sort-direction-char-normal ?▼
  "The character used to indicate that a column has been sorted chronologically."
  :type  'character
  :group 'mizik)

(defcustom mizik-sort-direction-char-reverse ?▲
  "The character used to indicate that a column has been sorted in reverse chrono."
  :type  'character
  :group 'mizik)

(defface mizik-column-color-title
  '((t :foreground "green"))
  "Face for numbering the visible cards in a stack for the user to select one."
  :group 'mizik)

(defface mizik-column-color-album
  '((t :slant italic :foreground "cyan"))
  "Face for numbering the visible cards in a stack for the user to select one."
  :group 'mizik)

(defface mizik-column-color-artist
  '((t :foreground "yellow"))
  "Face for numbering the visible cards in a stack for the user to select one."
  :group 'mizik)

(defface mizik-currently-playing
  '((t :weight bold))
  "Face for indicating which song in a Mizik buffer is currently playing."
  :group 'mizik)

(defface mizik-filter-tag-sanction
  '((t :inherit font-lock-function-name-face :weight bold))
  "Face for keywords that require values for a specific tag."
  :group 'mizik)

(defface mizik-filter-tag-negation
  '((t :inherit font-lock-builtin-face :weight bold))
  "Face for keywords that avoid values for a specific tag."
  :group 'mizik)

(defface mizik-filter-tag-regexp-sanction
  '((t :inherit font-lock-constant-face :weight bold))
  "Face for keywords that require values, discerned via regex., for a specific tag."
  :group 'mizik)

(defface mizik-filter-tag-regexp-negation
  '((t :inherit mizik-filter-tag-negation))
  "Face for keywords that avoid values, discerned via regex., for a specific tag."
  :group 'mizik)

(defface mizik-filter-logical-operators
  '((t :inherit font-lock-variable-name-face :slant italic))
  "Face for logical operator keywords in a Mizik filter string."
  :group 'mizik)

(defface mizik-filter-limit
  '((t :inherit font-lock-type-face :weight bold))
  "Face for result limit keyword in a Mizik filter string."
  :group 'mizik)

(defmacro mizik--save-excursion (&rest body)
  "Like `save-excursion', execute BODY and return to the song the point was on.

This is the only major difference from `save-excursion': rather than returning
to the original point, `mizik--save-excursion' takes account of which song is
under the point to return to wherever that song now is in the buffer, after
executing BODY."
  (declare (indent 0) (debug t))

  (let ((index (gensym)))
    `(let ((,index (get-text-property (point) 'original-index)))
       (unwind-protect
           ,@body
         (goto-char (if ,index (text-property-any (point-min)     (point-max)
                                                  'original-index ,index)
                               (point-min)))

         ;; to fully work, `switch-to-buffer-preserve-window-point' must be disabled
         (when-let ((window (get-buffer-window (current-buffer))))
           (set-window-point window (point)))))))

(defmacro mizik--inhibit-read-only (&rest body)
  "Enable `inhibit-read-only' for the duration of BODY."
  (declare (indent 0) (debug t))

  `(let ((inhibit-read-only t))
     ,@body))

(defun mizik--move-to-visible-line ()
  "Find the nearest visible line and move the point to the beginning of it."

  (if-let ((p (text-property-any (point) (point-max) 'invisible nil)))
      ;; when not at the end of the buffer, we don't want to end up on an
      ;; invisible line so try to move to the next visible line we can
      (goto-char p)
    ;; when the cursor was, originally, on a line that has become hidden (and
    ;; that line was one of the last of the buffer), the cursor ends up looking
    ;; like it's at the end of the visible line once the original line is
    ;; hidden; this doesn't disrupt functionality but may confuse the user so we
    ;; fix it, visually
    (ignore-errors
      (previous-line)
      (move-beginning-of-line 1))))

(defun mizik--find-mizik-buffers ()
  "Find and return all Mizik buffers currently open."

  (cl-remove-if-not (lambda (buffer)
                      (let ((name (buffer-name buffer)))
                        (or (string-equal mizik---buffer-name-library name)
                            (string-equal mizik---buffer-name-queue   name)
                            (string-match (replace-regexp-in-string
                                            "{}"
                                            ".+"
                                            mizik---buffer-name-playlist-format)
                                          name))))
                    (buffer-list)))

(defmacro mizik--pair-tag-and-process-function (symbol &rest body)
  "Create a pair of SYMBOL and a function to transform the tag value of a song.

The SYMBOL must belong to a song-file tag's name while the function will assign
the value of that tag for the song resource (that has been passed to the
function) to a variable named \"it\".

BODY will, then, make up the rest of the code of the function."
  (declare (indent 1) (debug t))

  `(cons ,symbol (lambda (plst) (let ((it (plist-get plst ,symbol))) ,@body))))
(defun mizik--format-code-to-tag (code)
  "For format CODE, return the corresponding MPD tag and string function as a pair.

The MPD tag can be used to retrieve the tag's value from song metadata (likely
through `plist-get').

The string function is used to convert the received value, from MPD, into a
string, if it isn't one – already –, or, in either case, for format the value
into a more appropriate form (like turning the amount of seconds for a song's
runtime into a hours/minutes/seconds form).

This function is primarily used to handle the format codes in
`mizik-mode-line-marquee-format', `mizik-mode-line-static-preceding-format',
`mizik-mode-line-static-succeeding-format', and `mizik-songs-format-string'."

  (cl-case (intern code)
    ;; Codes in song resource
    ((%fi quote) (mizik--pair-tag-and-process-function 'file        it))
    ((%ar quote) (mizik--pair-tag-and-process-function 'Artist      it))
    ((%al quote) (mizik--pair-tag-and-process-function 'Album       it))
    ((%ti quote) (mizik--pair-tag-and-process-function 'Title       it))
    ((%tr quote) (mizik--pair-tag-and-process-function 'Track       it))
    ((%ge quote) (mizik--pair-tag-and-process-function 'Genre       it))
    ((%di quote) (mizik--pair-tag-and-process-function 'Disc        it))
    ((%aa quote) (mizik--pair-tag-and-process-function 'AlbumArtist it))
    ((%tm quote) (mizik--pair-tag-and-process-function 'Time
                   (when it
                     (format-seconds (concat (if (< it 3600) "" "%.2h:")
                                             "%.2m:%.2s")
                                     it))))
    ((%du quote) (mizik--pair-tag-and-process-function 'duration
                   (when it
                     (let ((num (string-to-number it)))
                       (format-seconds (concat (if (< num 3600) "" "%.2h:")
                                               "%.2m:%.2s")
                                       num)))))
    ;; Codes in MPD status
    ((%te quote) (mizik--pair-tag-and-process-function 'time-elapsed
                   (when it
                     (format-seconds (concat (if (< it 3600) "" "%.2h:")
                                             "%.2m:%.2s")
                                     it))))
    ((%tt quote) (mizik--pair-tag-and-process-function 'time-total
                   (when it
                     (format-seconds (concat (if (< it 3600) "" "%.2h:")
                                             "%.2m:%.2s")
                                     it))))
    ((%st quote) (mizik--pair-tag-and-process-function 'state
                   (cl-case it
                     ((play  quote) (if mizik-mode-line-unicode-p "▶"  "P"))
                     ((pause quote) (if mizik-mode-line-unicode-p "||" "p"))
                     ((stop  quote) (if mizik-mode-line-unicode-p "■"  "S")))))
    ((%re quote) (mizik--pair-tag-and-process-function 'repeat
                   (cl-case it
                     (0 (if mizik-mode-line-unicode-p "→" ""))
                     (1 (if mizik-mode-line-unicode-p "⟲" "R")))))
    ((%si quote) (mizik--pair-tag-and-process-function 'single
                   (cl-case (or (and (stringp it) (intern it)) "")
                     ((\0      quote) "")
                     ((\1      quote) (if mizik-mode-line-unicode-p "¹" "1"))
                     ((oneshot quote) (if mizik-mode-line-unicode-p "⁰" "0")))))
    ((%sh quote) (mizik--pair-tag-and-process-function 'random
                   (cl-case it
                     (0 (if mizik-mode-line-unicode-p "⇉" ""))
                     (1 (if mizik-mode-line-unicode-p "⤭" "s")))))))

(defun mizik--group-string (regexp string)
  "Get a list of all REGEXP matches in STRING.

Primarily, used to parse `mizik-songs-format-string'."

  (save-match-data
    (let ((pos 0)
          matches)
      (while (string-match regexp string pos)
        (push (match-string 0 string) matches)

        (setq pos (match-end 0)))

      (nreverse matches))))

(defun mizik--update-playing-visual ()
  "Update which line in the `mizik---currently-playing-buffer' is now playing.

If the buffer is currently open, unmark which line was marked as playing in the
buffer and, then, mark the line which corresponds to the song MPD is playing."

  (when-let* ((status  (mpd-get-status mpd-inter-conn))
              (state   (not (eq (plist-get status 'state) 'stop)))
              (current (and mizik---currently-playing-buffer
                            (get-buffer mizik---currently-playing-buffer))))
    (with-current-buffer current
      (mizik--inhibit-read-only
        (save-excursion
          ;; remove old one
          (font-lock--remove-face-from-text-property (point-min)
                                                     (point-max)
                                                     'face
                                                     'mizik-currently-playing)

          ;; add new one
          (when-let ((pnt (text-property-any (point-min)     (point-max)
                                             'queue-position (plist-get status 'song))))
            (goto-char pnt)

            (add-face-text-property pnt (pos-eol) 'mizik-currently-playing)))))))

(defun mizik--timer-handler ()
  "The function used by `mizik---mode-line-timer' to update the modeline.

The current status of MPD and the currently playing song are first fetched.

`mizik-mode-line-static-preceding-format', `mizik-mode-line-marquee-format',
and `mizik-mode-line-static-succeeding-format' have their format codes replaced,
based off of the status and current song, and, then,
`mizik-mode-line-marquee-format' is narrowed down to the length of
`mizik-mode-line-marquee-length' and sets the result to
`mizik---mode-line-marquee-visible-text'.

`mizik-mode-line-static-preceding-format',
`mizik---mode-line-marquee-visible-text', and
 `mizik-mode-line-static-succeeding-format' are then, respectively, concatenated
to set `mizik---mode-line-status' to update the modeline."

  (let* ((status
               (mpd-get-status mpd-inter-conn))
         (song
               (car (mpd-get-playlist-entry mpd-inter-conn
                                            (plist-get status 'song))))
         (code-replace
              (lambda (formatString)
                (let ((newCurrentText formatString))
                  (mapc (lambda (code)
                          (setq newCurrentText
                                (string-replace
                                  code
                                  (let ((tag (mizik--format-code-to-tag code)))
                                    (or (funcall (cdr tag) song)
                                        (funcall (cdr tag) status)
                                        "<Unknown>"))
                                  newCurrentText)))
                        (mizik--group-string "%[a-z]\\{2\\}" formatString))

                  newCurrentText))))
    (when-let* ((elapsed            (plist-get status 'time-elapsed))
                (        (<= elapsed mizik-mode-line-timer-interval)))
      (mizik--update-playing-visual))

    (let ((newCurrentText (funcall code-replace mizik-mode-line-marquee-format)))
      (setq mizik---mode-line-marquee-visible-text
            (let ((index (string-match (regexp-quote
                                         mizik---mode-line-marquee-visible-text)
                                       newCurrentText))
                  (len   (length newCurrentText)))
              (if (not index)
                  (progn
                    (setq mizik---mode-line-marquee-scroll-forward-p nil)

                    (substring newCurrentText
                               0
                               (min len mizik-mode-line-marquee-length)))
                (if (>= mizik-mode-line-marquee-length len)
                    newCurrentText
                  (when (or (zerop index)
                            (zerop (- len (+ index
                                             mizik-mode-line-marquee-length))))
                    (setq mizik---mode-line-marquee-scroll-forward-p
                          (not mizik---mode-line-marquee-scroll-forward-p)))

                  (let ((1+- (if mizik---mode-line-marquee-scroll-forward-p
                                 #'1+
                               #'1-)))
                    (substring newCurrentText
                               (funcall 1+- index)
                               (+ (funcall 1+- index)
                                  mizik-mode-line-marquee-length))))))))

    (setq mizik---mode-line-status
          (concat (funcall code-replace mizik-mode-line-static-preceding-format)
                  mizik---mode-line-marquee-visible-text
                  (funcall code-replace mizik-mode-line-static-succeeding-format))))

  (force-mode-line-update))

(defun mizik--on-buffer-kill (&optional allp)
  "Save buffer if `mizik---currently-playing-buffer' (check all buffers, if ALLP).

All files of form \".mizik-<buffer name>\" are remoed from the home directory
and, then, all text-properties of each line – sorted by text-property
\\='original-index, in case the buffer has any of their columns sorted – are
saved to \"~/.mizik-`mizik---currently-playing-buffer'\".

This function is utilized in `kill-buffer-hook' so that, if the user kills a
buffer from which MPD is playing, Mizik can properly render the play-information
\(such as which song MPD is currently playing) once that buffer is reopened.

The option to cycle through all currently-open Mizik buffers (with ALLP) is for
`mizik--on-emacs-kill' which is used in `kill-emacs-hook' since
`kill-buffer-hook' isn't called when one tries to close Emacs."

  (if-let ((mizikBuffers (mizik--find-mizik-buffers)))
      (mapc (lambda (buffer)
              (with-current-buffer buffer
                (let ((bName (buffer-name buffer)))
                  (message "Closing Mizik buffer…")

                  (remhash bName mizik---buffer-filters)

                  (when (string-equal bName mizik---currently-playing-buffer)
                    (let ((bs (buffer-string)))
                      (mapc #'delete-file (directory-files "~" t "\\.mizik.*"))

                      (with-temp-file (concat "~/.mizik-" bName)
                        (insert "'")
                        (insert (prin1-to-string
                                  (sort (mapcar (lambda (line)
                                                  (text-properties-at 0 line))
                                                (nbutlast (string-split bs "\n")))
                                        (lambda (l1 l2)
                                          (< (plist-get l1 'original-index)
                                             (plist-get l2 'original-index))))))))))))
            (if allp mizikBuffers (list (current-buffer))))
    (remove-hook 'kill-buffer-hook #'mizik--on-buffer-kill)))
(defun mizik--on-emacs-kill ()
  "Find the `mizik---currently-playing-buffer' and save its text-properties.

Does this by calling `mizik--on-buffer-kill' with \\='t\\=' for ALLP."

  (mizik--on-buffer-kill t))

(defun mizik--format-string-to-column-data ()
  "Break `mizik-songs-format-string' into column data for displaying songs.

This data can be used to turn a list of songs into formatted strings to `insert'
into a buffer (such as with `mizik--format-songs-into-buffer').

The final result is a list of lists (one for each column specified by the user).

The first element in the sublist is a pair of the tag of the column as a symbol
\(such as \"Artist\", \"Album\", \"Title\", etc.) and the function to call on
the value of this column to get a formatted string to use in rendering; these
are derived via `mizik--format-code-to-tag'.

The second element is whether the user wants to truncate this column by
percentage of the window's width or by character-count; this value is \\='t\\=',
for the former, and \\='nil\\=', for the latter.

The third element is the numerical value (for either a percentage or
character-count) specified by the user for this column.

The fourth element is any trailing characters the user wants to go right before
the next column.

The fifth element is the max length that this column should be, based off of the
third element and the width of the window that this function is called from."

  (let* ((cols (mapcar (lambda (col)
                         (let* ((t&w   (car (mizik--group-string "^...-?[0-9]\\{1,2\\}"
                                                                 col)))
                                (tag   (mizik--format-code-to-tag (substring t&w 0
                                                                                 3)))
                                (width (substring t&w 3))
                                (end   (substring col (length t&w))))
                           (list tag
                                 (not (string-prefix-p "-" width))
                                 (string-to-number
                                   (substring width (string-match-p "[0-9]+" width)))
                                 end)))
                       (mizik--group-string "%[a-z]\\{2\\}-?[0-9]\\{1,2\\}[^%]*"
                                            mizik-songs-format-string)))
         (ends (cl-reduce #'+ (mapcar (lambda (col)
                                        (+ (if (cl-second col) 0 (cl-third col))
                                           (length (cl-fourth col))))
                                      cols)))
         (wdth (window-total-width (get-buffer-window (current-buffer)))))
    (mapcar (lambda (col)
              (append col (list (if (cl-second col)
                                    (floor
                                      (* (/ (cl-third col) 100.0) (- wdth ends)))
                                  (cl-third col)))))
            cols)))

(defun mizik--format-song-to-string (columns window-length &optional no-colorize-p)
  "Use COLUMNS data to convert a song to a string no longer than WINDOW-LENGTH.

NO-COLORIZE-P determines whether to apply a face to any of the text (this is
largely used by `mizik--header-line-update-sort-marker' so that the
`header-line-format' doesn't have any of the faces that the song in their
respective columns do).

COLUMNS data is derived from `mizik--format-string-to-column-data'.

This function returns a `lambda' so that its operations can be run by an
iterative function such as `cl-mapc'.

This needed to be split in this way as the COLUMNS data and WINDOW-LENGTH would
drastically slow down how quickly this function runs if they were to be
recomputed every time the function processed a song (particularly if the number
of songs which were to be processed was large).

The `lambda' takes three arguments: SONG, INDEX, and QUEUE-POSITION.

SONG is the song metadata in an MPD resource.

INDEX is the order of the songs, used to keep track of the original ordering –
when the songs were first loaded into the buffer – in case the user sorts the
songs in the buffer (at any point).

QUEUE-POSITION is provided to map the songs to their position in the current
MPD Queue; this is primarily used by `mizik--on-window-resize' – so that this
information isn't lost when readjusting the songs to fit in a buffer of
different WINDOW-LENGTH – and, during the process of `mizik--load-buffer', when
loading saved data because the buffer being loaded was the
`mizik---currently-playing-buffer'."

  (byte-compile
    (lambda (song index queue-position invisibilityp)
      (let ((begOfLinePoint (point)))
        (dolist (col columns)
          (let* ((txt    (funcall (cdr (cl-first col)) song))
                 (txtLen (length txt))
                 (begPnt (point)))
            (when txt (insert txt))

            (if (< txtLen (cl-fifth col))
                (insert (make-string (- (cl-fifth col) txtLen) ? ))
              (backward-char (- txtLen (- (cl-fifth col) 3)))
              (kill-line)
              (insert "…  "))

            (insert (cl-fourth col))

            (put-text-property
              begPnt (point)
              'face  (if no-colorize-p
                         nil
                       (cl-case (car (cl-first col))
                         ((Artist quote) 'mizik-column-color-artist)
                         ((Album  quote) 'mizik-column-color-album)
                         ((Title  quote) 'mizik-column-color-title))))))

        ;; After splitting the screen twice, `window-total-width'
        ;; reported a width 2 characters more than what could fit
        ;; in the window/buffer (without overflowing or breaking
        ;; to the next line); splitting more than this didn't
        ;; continue to increase the extra count
        ;; We have two extra spaces at the end, anyway, and don't
        ;; need them to indicate separation from another column
        ;; (since it's the last column) so shave off 2 characters
        ;; to ensure we fit the buffer
        ;; In graphical mode, things like scrollbars cut into
        ;; what will be visible in the buffer, as well, so cut a
        ;; little more
        (let ((sub (- (- (point)       begOfLinePoint)
                      (- window-length (if (display-graphic-p) 4 2)))))
          (when (natnump sub)
            (backward-delete-char sub)))

        (add-text-properties begOfLinePoint
                             ;; doing the entire line slows the operation down
                             (1+ begOfLinePoint)
                             `(song-data      ,song
                               window-length  ,window-length
                               original-index ,index
                               queue-position ,queue-position))

        (newline)

        ;; don't do this unless we want to change the default
        ;; applying to the whole line is slow
        (when invisibilityp
          (put-text-property begOfLinePoint (point) 'invisible t))))))

(defun mizik--header-line-update-sort-marker (column-key columns-data)
  "Generate `header-line-format' based off of COLUMNS data, sorted by COLUMN-KEY.

COLUMNS-DATA is derived from `mizik--format-string-to-column-data' while
COLUMN-KEY is a symbol used to identify which column is being chosen for
sorting.

While the function name mentions updating, the sort marker is only updated when
COLUMN-KEY is non-\\='nil\\='; if \\='nil\\=', the sort character
\(`mizik-sort-direction-char-normal', `mizik-sort-direction-char-reverse', or
just a space) that was last used for the last sorted column is used, once again.

Previous sorting data is stored as text-properties under \\='sorted-column and
\\='sorting-character; this allows both to be able to preserve a column's
current sorting character, if COLUMN-KEY is \\='nil\\=', and to figure out what
the sorting characer should be if a new column is sorted or the COLUMN-KEY is
the same column as the previously sorted column."

  (let* ((chOrder (list mizik-sort-direction-char-normal
                        mizik-sort-direction-char-reverse ? ))

         (hlProps (text-properties-at 0 (or header-line-format "")))
         (prevSC  (plist-get hlProps 'sorting-character))
         (pColKey (plist-get hlProps 'sorted-column))

         (nColKey (or column-key pColKey))
         ( newSC  (cond
                   ((null       column-key) (or prevSC (car (last chOrder))))
                   ((eq pColKey column-key) (elt chOrder
                                                 (% (1+ (cl-position prevSC
                                                                     chOrder))
                                                    (length chOrder))))
                   (t                       (car chOrder))))

         (index   (let ((tmp 0))
                    (cl-find-if (lambda (col)
                                  (setq tmp (+ tmp (cl-fifth col)))

                                  (let ((verdict (eq (car (cl-first col)) nColKey)))
                                    (unless verdict
                                      (setq tmp (+ tmp (length (cl-fourth col)))))

                                    verdict))
                                columns-data)

                    tmp))

         (newHL   (with-temp-buffer
                    (funcall (mizik--format-song-to-string columns-data
                                                           (window-total-width
                                                             (get-buffer-window
                                                              (current-buffer)))
                                                           t)
                             '(Album  "Album"
                               Title  "Title"
                               Artist "Artist")
                             1
                             nil
                             nil)
                    (backward-delete-char 1)

                    (buffer-string))))
    (setq header-line-format (concat (substring newHL 0 (- index 2))
                                     (list newSC)
                                     (substring newHL (min (1- index)
                                                           (length newHL)))))

    (add-text-properties 0 1 `(sorted-column     ,nColKey
                               sorting-character ,newSC)   header-line-format)

    (if (eq newSC ? ) nil newSC)))

(defun mizik--format-songs-into-buffer (songs &optional   indices
                                              q-positions invisibilities)
  "Format SONGS into a buffer, largely via repeated `mizik--format-song-to-string'.

`mizik--format-songs-into-buffer' aims to be the main function by which SONGS
get inputted into the current buffer (from formatting the truncating of the text
of each column into the appropriate size to the faces necessary for each column)
and, so, also takes care of inhibiting `read-only-mode' (via `inhibit-read-only'
\(via `mizik--inhibit-read-only')) and even erases the buffer, in case it's
being called because the width of the window has changed and the text needs to
be resized.

In the case of reloading a `mizik---currently-playing-buffer' or when called by
`mizik--on-window-resize' because the window width has changed, it can also be
passed a list of INDICES, queue positions (Q-POSITIONS), and INVISIBILITIES (a
list of booleans saying whether the line is invisible or not) so that these can
get saved as text-properties to the appropriate lines in the buffer."

  (mizik--inhibit-read-only
    (erase-buffer)

    (let ((cols   (mizik--format-string-to-column-data))
          (winLen (window-total-width (get-buffer-window (current-buffer))))
          (sngLen (length songs)))
      (mizik--header-line-update-sort-marker nil cols)

      (cl-mapc (mizik--format-song-to-string cols winLen)
               songs
               (or indices        (number-sequence 1      sngLen))
               (or q-positions    (make-list       sngLen nil))
               (or invisibilities (make-list       sngLen nil)))

      ;; Adds 0.02–0.04 seconds to a 15,000-song buffer but, like, we need this
      (when (string-equal mizik---currently-playing-buffer (buffer-name))
        (mizik--update-playing-visual))))

  (goto-char (point-min)))

(defmacro mizik--load-buffer (buffer songs mode &optional switchp)
  "Load SONGS into new BUFFER and activate MODE, if uncreated; if SWITCHP, switch.

In the case of creating a new BUFFER, check whether the Playlist/Library that
the BUFFER represents was/is playing, before, when loading SONGS into BUFFER.

BUFFER is a string (the buffer's name) while SONGS is a list of MPD resources
with metadata.

Whenever trying to open a BUFFER to put SONGS in, it should be checked as to
whether Mizik marked the Playlist or the MPD Library to which this BUFFER
corresponds as the last-playing thing before Mizik or this particular buffer had
been closed.

If it is, load this information from a file stored as \"~/.mizik-BUFFER\"; said
file will have all the song metadata as well as the original ordering and, even,
the position of each song in the MPD Queue.  Assuming the user didn't modify the
Queue or the Playlist, itself, while Mizik was closed, this should allow Mizik
to resume whatever state MPD has been in, should MPD have kept playing while
Mizik was closed.

If this information is not available, assume that the SONGS the caller of this
function has provided must be used – instead – to fill the BUFFER.

This was created as a macro, rather than a function, because (especially for
large Libraries) the code for fetching SONGS may take a bit to execute.  A macro
allows us to run SONGS only once we've determined that this information hasn't,
already, been saved for us and is ready to just import into the buffer named
BUFFER."
  (declare (debug t))

  (let ((data (gensym))
        (buff (gensym))
        (save `(concat "~/.mizik-" ,buffer)))
    `(let ((,buff (get-buffer ,buffer)))
       (get-buffer-create ,buffer)

       (when (not ,buff)
         (with-current-buffer ,buffer
           (,mode)

           (puthash (buffer-name) "" mizik---buffer-filters)
           (setq-local global-disable-point-adjustment        t
                       ;; to fully work, buffers in windows must `set-window-point'
                       switch-to-buffer-preserve-window-point nil)
           (buffer-disable-undo)
           (toggle-truncate-lines t)

           (if (file-exists-p ,save)
               (let ((,data (eval (read-from-whole-string
                                    (with-temp-buffer
                                      (insert-file-contents ,save)

                                      (buffer-string))))))
                 (setq mizik---currently-playing-buffer ,buffer)

                 (mizik--format-songs-into-buffer
                   (mapcar (lambda (line) (plist-get line 'song-data))      ,data)
                   (mapcar (lambda (line) (plist-get line 'original-index)) ,data)
                   (mapcar (lambda (line) (plist-get line 'queue-position)) ,data)))
             (mizik--format-songs-into-buffer ,songs))))

       (when ,switchp (switch-to-buffer ,buffer)))))

(defun mizik--on-window-resize (_frame)
  "Call `mizik--format-songs-into-buffer' when the window's width has changed."

  (if-let ((buffers (mizik--find-mizik-buffers)))
      ;; `dolist' .4 seconds faster than `mapc'
      (dolist (buffer buffers)
        (when-let ((window (get-buffer-window buffer)))
          (with-current-buffer buffer
            (unless (= (window-total-width window)
                       (get-text-property (point-min) 'window-length))
              (mizik--save-excursion
                (let ((indices '()) (qPoses '()) (invises '()))
                  (mizik--format-songs-into-buffer
                    (mapcar (lambda (line)
                              (let ((props (text-properties-at 0 line)))
                                (push (plist-get props 'original-index) indices)
                                (push (plist-get props 'queue-position) qPoses)
                                (push (plist-get props 'invisible)      invises)

                                (plist-get props 'song-data)))
                            (nbutlast (string-split (buffer-string) "\n")))
                    (nreverse indices)
                    (nreverse qPoses)
                    (nreverse invises))))))))
    (remove-hook 'window-size-change-functions #'mizik--on-window-resize)))

(defun mizik--filter-parse (filter)
  "Parse a string, FILTER, into a plist which tell whether a song should be hidden."

  (let ((mustHaves '())
        (mustNots  '())
        (limit     nil))
    (mapc (lambda (tagAndValue)
            (cl-case (aref tagAndValue 0)
              (?@ (let* ((splitInt (string-match split-string-default-separators
                                                 tagAndValue))
                         (modifier (aref tagAndValue 1))
                         (tag      (substring (downcase tagAndValue)
                                              (if (or (= modifier ?!)
                                                      (= modifier ?=)
                                                      (= modifier ?~)) 2 1)
                                              splitInt))
                         (value    (string-split (substring tagAndValue splitInt)
                                                 "$or")))
                    (push (cons (cl-case (intern tag)
                                  ((file         quote) 'file)
                                  ((artist       quote) 'Artist)
                                  ((album        quote) 'Album)
                                  ((title        quote) 'Title)
                                  ((track        quote) 'Track)
                                  ((genre        quote) 'Genre)
                                  ((disc         quote) 'Disc)
                                  ((album-artist quote) 'AlbumArtist)
                                  ((time         quote) 'Time))
                                (mapcar #'string-trim value))
                          (if (= modifier ?!) mustNots mustHaves))))
              (?# (setq limit (string-to-number (substring tagAndValue 1))))
              ))
          (mizik--group-string "@[^@#[:space:]]+[[:space:]]+[^@#]+\\|#[0-9]+" filter))

    `(,@(when mustHaves (list 'haves mustHaves))
      nots ,(or mustNots '(nil))
      ,@(when limit     (list 'limit limit)))))

(defun mizik--filter-search-forward (filter-data &optional currentp)
  "Search forward, in the buffer, for the next song which matches FILTER-DATA.

FILTER-DATA should have come from `mizik--filter-parse'.
`mizik--filter-search-forward' passes a `lambda' to
`text-property-search-forward' (as the PREDICATE argument) which uses
FILTER-DATA to determine if the regions `text-property-search-forward' searches
through corresponds to song which should be hidden.  If yes, the line in the
buffer is given a text-property of \\='invisible.

CURRENTP determines if the current region the point is on should be included in
the search."

  (text-property-search-forward
    'song-data
    filter-data
    (lambda (filter song)
      (if (null song)
          ;; `text-property-search-forward' covers every text-property range
          ;; that changes which means including the part of each line which have
          ;; no text-properties for \\='song-data, \\='queue-position, etc. In
          ;; these cases, saying we want to keep the line which marks the rest
          ;; of an invisible line visible is redundant and marking the rest of a
          ;; visible line invisible is counter-productive
          nil
        (or (cl-some  (lambda (have)
                        (not (when-let ((v (plist-get song (car have))))
                               (cl-some (lambda (sub)
                                          (string-match-p (regexp-quote sub) v))
                                        (cdr have)))))
                      (plist-get filter 'haves))
            (cl-every (lambda (no)
                        (when-let ((v (plist-get song (car no))))
                          (cl-some (lambda (sub)
                                     (string-match-p (regexp-quote sub) v))
                                   (cdr no))))
                      (plist-get filter 'nots)))))
    (not currentp)))
(defun mizik--filter-buffer ()
  "Update the current buffer based on the current filter string.

When the minibuffer – to edit the filter string – is open, update the
text-property colors."

  (when (and mizik---filter-actively-edited
             (not (string= mizik---filter-actively-edited
                           (minibuffer-contents-no-properties))))
    (save-excursion
      (remove-text-properties (+ (point-min) (length "Filter: "))
                              (point-max)
                              '(face))

      (goto-char (point-min))
      (while (re-search-forward "@[^@!~=#[:space:]][^@#[:space:]]+" nil t)
        (put-text-property (match-beginning 0)
                           (match-end       0)
                           'face 'mizik-filter-tag-sanction))

      (goto-char (point-min))
      (while (re-search-forward "@![^@#[:space:]]+" nil t)
        (put-text-property (match-beginning 0)
                           (match-end       0)
                           'face 'mizik-filter-tag-negation))

      (goto-char (point-min))
      (while (re-search-forward (regexp-quote "$or") nil t)
        (put-text-property (match-beginning 0)
                           (match-end       0)
                           'face 'mizik-filter-logical-operators))

      (goto-char (point-min))
      (while (re-search-forward "#[0-9]+" nil t)
        (put-text-property (match-beginning 0)
                           (match-end       0)
                           'face 'mizik-filter-limit)))

    (setq mizik---filter-actively-edited (minibuffer-contents-no-properties)))

  (with-current-buffer (or (and (minibuffer-selected-window)
                                (window-buffer (minibuffer-selected-window)))
                           (current-buffer))
    (mizik--save-excursion
      (mizik--inhibit-read-only
        (remove-text-properties (point-min) (point-max) '(invisible))

        (goto-char (point-min))

        (let* ((data   (mizik--filter-parse (or mizik---filter-actively-edited
                                                (gethash (buffer-name)
                                                         mizik---buffer-filters))))
               (foundp (mizik--filter-search-forward data t)))
          (while foundp
            (put-text-property (pos-bol) (1+ (pos-eol)) 'invisible t)

            (setq foundp (mizik--filter-search-forward data)))

          (when-let* ((limit                           (plist-get data 'limit))
                      (      (< limit (count-lines (point-min) (point-max) t))))
            (goto-char (point-min))

            (dotimes (_ limit)
              (next-line))

            (put-text-property (pos-bol) (point-max) 'invisible t)))))

    (mizik--move-to-visible-line)))

(defun mizik-filter-completion-at-point ()
  "This is the function to be used for the hook `completion-at-point-functions'."
  (interactive)

  (let* ((bds   (bounds-of-thing-at-point 'symbol))
         (start         (if bds (car bds) (point))))
    (list start
          (if bds (cdr bds) (point))
          (cl-case (intern (buffer-substring-no-properties start
                                                           (min (point-max)
                                                                (+ start 2))))
            ((@! quote) mizik---filter-keywords-negate)
            ((@= quote) mizik---filter-keywords-regexp)
            ((@~ quote) mizik---filter-keywords-regexp-negate)
            (t          mizik---filter-keywords-all))
          . nil)))

(defvar mizik-filter-syntax-table
  (let ((table (make-syntax-table)))
    (prog1 table
      (modify-syntax-entry ?# "w" table)
      (modify-syntax-entry ?@ "w" table)
      (modify-syntax-entry ?! "w" table)
      (modify-syntax-entry ?= "w" table)
      (modify-syntax-entry ?~ "w" table)))
  "Syntax table active when editing a Mizik buffer's filter in the minibuffer.")

(defun mizik--on-minibuffer-setup ()
  "The necessary code, when the minibuffer is opened, for it to behave as needed.

While this function is intended for any minibuffer Mizik might need, – right
now – it only affects the live/active filtering minibuffer as that's the only
instance, currently, where Mizik needs custom behavior."

  (if (not mizik---filter-actively-edited)
      (unless (mizik--find-mizik-buffers)
        (remove-hook 'minibuffer-setup-hook #'mizik--on-minibuffer-setup))
    (set-syntax-table mizik-filter-syntax-table)

    (add-hook 'completion-at-point-functions #'mizik-filter-completion-at-point nil t)
    (add-hook 'post-command-hook             #'mizik--filter-buffer             nil t)))

(defvar mizik-filter-mode-map (let ((mode-map (copy-keymap minibuffer-local-map)))
                                (define-key mode-map (kbd "TAB") 'completion-at-point)

                                mode-map)
  "Keymap for editing the filter of a Mizik buffer in the minibuffer.")

(defun mizik-filter-modify ()
  "Edit the buffer's filter via the minibuffer; save to `mizik---buffer-filters'."
  (interactive)

  (let ((name (buffer-name)))
    (unwind-protect
        (let ((mizik---filter-actively-edited (gethash name
                                                       mizik---buffer-filters)))
          (puthash name
                   (read-from-minibuffer "Filter: "
                                         mizik---filter-actively-edited
                                         mizik-filter-mode-map)
                   mizik---buffer-filters))
      (mizik--filter-buffer))))

(defun mizik--sort-lines-by-text-property (reversep key1 &optional key2)
  "Sort a buffer's lines by text-property.

The text-property used for sorting is gathered by calling `plist-get' on the
text-properties of the line with KEY1 provided as the property; if KEY2 is
non-\\='nil\\=', call `plist-get' again, on the retrieved value but with KEY2
provided as the property of this `plist-get' call.

If REVERSEP is non-\\='nil\\=', the lines are rearranged in order of descending
sort key.

In order to not disrupt any invisibility applied by `mizik--filter-buffer', the
bounds are from the beginning of a line to the newline at the end.  For this
reason, NEXTRECFUN needed to not move the point while ENDRECFUN needed to get to
the beginning of the next line.  `+', while not relevant to points, was used for
NEXTRECFUN, for this reason (`forward-line' for ENDRECFUN should be self-
explanatory)."

  (mizik--inhibit-read-only
    (mizik--save-excursion
      (let ((process (lambda (line)
                       (let ((property (get-text-property (car line) key1)))
                         (if key2 (plist-get property key2) property)))))
        (goto-char (point-min))

        (sort-subr reversep #'+ #'forward-line nil nil
                   (lambda (line1 line2)
                     (let ((prop1 (funcall process line1))
                           (prop2 (funcall process line2)))
                       (cl-typecase prop1
                         (number  (<       prop1 prop2))
                         (string  (string< prop1 prop2))
                         (boolean (or (and prop1 prop2) prop1))))))))))

(defun mizik--sort-buffer-by-column (column-key columns-data)
  "Sort the buffer by the column whose title matches the symbol COLUMN-KEY.

COLUMNS-DATA is the value returned by `mizik--format-string-to-column-data'.

This is a parameter, rather than being handled by this function, because all
places which call this function, so far, need the column data, to begin with,
and passing it to this function, rather than having this function call
`mizik--format-string-to-column-data', is quicker and more performant."

  (if-let ((sortingChar (mizik--header-line-update-sort-marker column-key
                                                               columns-data)))
      (mizik--sort-lines-by-text-property (eq sortingChar
                                              mizik-sort-direction-char-reverse)
                                          'song-data
                                          (or column-key
                                              (get-text-property 0
                                                                 'sorted-column
                                                                 header-line-format)))
    (mizik--sort-lines-by-text-property nil 'original-index)))

(defun mizik-sort-by-column (index)
  "Sort the current buffer by the values in the INDEXth column.

INDEX counts from one."
  (interactive "P")

  (let* ((errorTxt "Not a valid column index")
         (cols     (mizik--format-string-to-column-data))
         (colSymbs (mapcar #'caar cols))
         (colNames (mapcar #'symbol-name colSymbs)))
    (if-let ((colKey (nth (let ((i (or index
                                       (1+ (cl-position (funcall
                                                          mizik-completing-read-function
                                                          "Sort which column? "
                                                          colNames)
                                                        colNames
                                                        :test #'string-equal)))))
                            (1- (cl-typecase i
                                  ((integer 1 *) i)
                                  (null          1)
                                  (list          (car i))
                                  (t             (error errorTxt)))))
                          colSymbs)))
        (mizik--sort-buffer-by-column colKey cols)
      (error errorTxt))))

(defun mizik-load-library ()
  "Switch to the existing Library buffer or, if it doesn't (yet) exist, create it."
  (interactive)

  (mizik--load-buffer mizik---buffer-name-library
                      (mpd-get-directory-songs mpd-inter-conn)
                      mizik-mode
                      t))
(defun mizik-load-playlist (name)
  "Load the songs of playlist NAME into a buffer."
  (interactive (list (funcall mizik-completing-read-function
                              "Load which playlist: "
                              (mapcan (lambda (elem)
                                        (if (string= (car elem) "playlist")
                                            (list (cdr elem))
                                          nil))
                                      (cdr (mpd-execute-command mpd-inter-conn
                                                                "listplaylists"))))))

  (mizik--load-buffer (replace-regexp-in-string "{}"
                                                name
                                                mizik---buffer-name-playlist-format)
                      (mpd-get-songs mpd-inter-conn
                                     (mpd-make-cmd-concat "listplaylistinfo" name))
                      mizik-playlist-mode
                      t))
(defun mizik-load-queue ()
  "Switch to the exisitng Queue buffer or, if it doesn't (yet) exist, create it."
  (interactive)

  (mizik--load-buffer mizik---buffer-name-queue '() mizik-queue-mode t))

(defun mizik--add-to-buffer (buffer song)
  "Add the MPD resource SONG to the BUFFER; resort and refilter said BUFFER, after."

  (let ((columns (mizik--format-string-to-column-data)))
    (with-current-buffer buffer
      (mizik--inhibit-read-only
        (mizik--save-excursion
          (insert (with-temp-buffer
                    (funcall (mizik--format-song-to-string
                               columns
                               (window-total-width (get-buffer-window
                                                     (current-buffer))))
                             song
                             (with-current-buffer buffer
                               (1+ (count-lines (point-min) (point-max))))
                             nil
                             ;; TODO: could we hide the line without having to
                             ;;   refilter the entire buffer…?
                             nil)

                    (buffer-string)))

          (mizik--sort-buffer-by-column nil columns)

          (mizik--filter-buffer))))))
(defun mizik-add-to-playlist ()
  "Add the song, under point, to a playlist of choice."
  (interactive)

  (if-let ((s (get-text-property (point) 'song-data))
           (p (funcall mizik-completing-read-function
                       "Add to which playlist: "
                       (cons "<create>"
                             (mapcan (lambda (elem)
                                       (if (string= (car elem) "playlist")
                                           (list (cdr elem))
                                         nil))
                                     (cdr (mpd-execute-command mpd-inter-conn
                                                               "listplaylists")))))))
      (let ((playlistName (if (string-equal p "<create>")
                              (read-string "Name of new Playlist? ")
                            p)))
        (mpd-execute-command mpd-inter-conn
                             (concat (mpd-make-cmd-concat "playlistadd" playlistName)
                                     " "
                                     (mpd-safe-string (plist-get s 'file))))

        (when-let ((buffer (get-buffer (replace-regexp-in-string
                                         "{}"
                                         playlistName
                                         mizik---buffer-name-playlist-format))))
          (mizik--add-to-buffer buffer s)))
    (message "No song to add, here!")))
(defun mizik-add-to-queue ()
  "Add the song, under point, to the Queue."
  (interactive)

  (if-let ((song (get-text-property (point) 'song-data)))
      (progn
        (mizik--load-buffer mizik---buffer-name-queue '() mizik-queue-mode)

        (mizik--add-to-buffer mizik---buffer-name-queue song))
    (message "No song to add, here!")))
(defun mizik-add-to-queue-advanced (uri title artist)
  "Add a song to the Queue by URI with provided TITLE and ARTIST.

This function can add URLs to stream as well as handle file URIs.

If the URI is a YouTube link, this function will check if \"youtube-dl\" or
\"yt-dlp\" is installed and convert the YouTube link to something MPD can
read.

While MPD cannot receive the TITLE and ARTIST info., it's used in the Queue
buffer so that the user can, at least, spot the song they've added and it won't
appear as a blank line; an album title of \"Streaming Links\" is always provided
to, also, make it easier to recognize which songs are not part of your Library."
  (interactive (list (read-string "URI: ")
                     (read-string "Title: ")
                     (read-string "Artist: ")))

  (mizik--load-buffer mizik---buffer-name-queue '() mizik-queue-mode)

  (mizik--add-to-buffer
    mizik---buffer-name-queue
    `(file   ,(if (not (string-match-p (concat "^http[s]?://"               "\\("
                                               "\\(www\\.\\)?youtube\\.com" "\\|"
                                               "youtu.be"                   "\\)/")
                                       uri))
                  uri
                (if-let ((exec (or (executable-find "yt-dlp")
                                   (executable-find "youtube-dl"))))
                    (string-trim
                      (shell-command-to-string
                        (concat exec " -f \"bestaudio[ext=m4a]/best\" -g " uri)))
                  (error (concat "You need youtube-dl or yt-dlp "
                                 "installed to handle that URL!"))))
      Title  ,title
      Artist ,artist
      Album  "Streaming Links")))

(defun mizik-play ()
  "Play song under current point and become `mizik---currently-playing-buffer'."
  (interactive)

      ;; in the event the user is in a buffer with no songs
      ;; or gotten onto the empty newline, at the end
  (if (or (get-text-property (point) 'invisible)
          (not (get-text-property (point) 'song-data)))
      (message "No song to play, here!")
    ;; for performance considerations and why this funtion is written the way it
    ;; is, see commits f469859cebfc35ac5e5e98f7faf071ffd9bc5541 through
    ;; e73ef24f2404e4c0885e958e33bee9fcaa6139d4
    (let ((lines (mapcar (lambda (line)
                           (let ((props (text-properties-at 0 line)))
                             (cons (plist-get (plist-get props
                                                         'song-data)
                                              'file)  ; file URI
                                   (plist-get props 'invisible))))
                         (nbutlast (string-split (buffer-string) "\n"))))
          (index (line-number-at-pos)))
      (when mizik---currently-playing-buffer
        (with-current-buffer mizik---currently-playing-buffer
          (mizik--inhibit-read-only
            (font-lock--remove-face-from-text-property (point-min)
                                                       (point-max)
                                                       'face
                                                       'mizik-currently-playing))))

      (when-let* ((files (mapcar #'car (cl-remove-if #'cdr lines)))  ; get file uri
                  (      (or (not (get-text-property (point) 'queue-position))
                             (not (equal (mapcar (lambda (file) (concat "file: " file))
                                                 files)  ; files in buffer = current queue?
                                         (mpd-get-playlist mpd-inter-conn))))))
        (setq mizik---currently-playing-buffer (buffer-name (current-buffer)))

        (mpd-clear-playlist mpd-inter-conn)

        (mpd-enqueue mpd-inter-conn files)

        (mizik--inhibit-read-only
          (let ((len (1+ (- (pos-eol) (pos-bol))))
                (ind                            0)
                (pos                           -1))
            (mapc (lambda (line)
                    (let ((i (1+ (* len (1- (cl-incf ind))))))
                      (put-text-property i               (1+ i)
                                         'queue-position (if (cdr line)  ; if invisible
                                                             nil
                                                           (cl-incf pos)))))
                  lines))))

      (mpd-play mpd-inter-conn (get-text-property (point) 'queue-position)))

    (mizik--timer-handler)))
(defun mizik-jump-to-playing ()
  "Move point to the line which has the song currently playing."
  (interactive)

  (if (not (plist-get (text-properties-at (point)) 'queue-position))
      (message "No songs in this buffer are playing!")
    (goto-char (text-property-any (point-min)
                                  (point-max)
                                  'queue-position
                                  (plist-get (mpd-get-status mpd-inter-conn)
                                             'song)))

    (message "Jumped to current song")))
(defun mizik-toggle-play ()
  "If playing, pause MPD; if paused, start playing."
  (interactive)

  (if (eq (plist-get (mpd-get-status mpd-inter-conn) 'state) 'play)
      (mpd-pause mpd-inter-conn)
    (mpd-play mpd-inter-conn)))

(defun mizik-cycle-repeat ()
  "Cycle through the various repeat settings.

The default state is for things not to repeat; if this function is called, the
state is switched to repeating the current Mizik buffer; if the function is
called again, the state is switched to repeating only the currently-playing
song.  Calling the function once more switches back to the default state."
  (interactive)

  (let* ((status (mpd-get-status mpd-inter-conn))
         (repeat      (plist-get status 'repeat))
         (single      (plist-get status 'single)))
    (cond
     ((and (zerop repeat) (string= single "0")) (mpd-toggle-repeat mpd-inter-conn))
     ((and (= repeat 1)   (string= single "0")) (mpd-toggle-single mpd-inter-conn))
     ((and (= repeat 1)   (string= single "1")) (mpd-toggle-repeat mpd-inter-conn)
                                                (mpd-toggle-single mpd-inter-conn))
     (t                                         (mpd-toggle-repeat mpd-inter-conn -1)
                                                (mpd-toggle-single mpd-inter-conn -1))))

  (mizik--timer-handler))

(defun mizik-toggle-shuffle ()
  "If not on shuffle mode, switch it on; if on shuffle mode, switch it off."
  (interactive)

  (mpd-toggle-random mpd-inter-conn)

  (mizik--timer-handler))

(defun mizik--seek (time)
  "Add TIME seconds to the currently-selected song's current timestamp."

  (let ((status (mpd-get-status mpd-inter-conn)))
    (mpd-seek mpd-inter-conn
              (plist-get status 'song)
              (max 0 (+ (plist-get status 'time-elapsed) time)))))
(defun mizik--seek-parse (time)
  "Determine the number of seconds TIME is meant to represent.

Because `mizik-seek-forward' and `mizik-seek-backward' utilize the prefix
argument, TIME may not always come through as an integer.

This function is used to convert the value into a usable integer."

  (cl-typecase time
    (number time)
    (null   5)
    (list   (car time))
    (t      (error "Given value for `time' is not valid"))))
(defun mizik-seek-forward (time)
  "Move the currently-selected song's current timestamp forward by TIME seconds.

`abs' is applied to TIME to ensure the used amount is always positive."
  (interactive "P")

  (mizik--seek (abs (mizik--seek-parse time))))
(defun mizik-seek-backward (time)
  "Move the currently-selected song's current timestamp backward by TIME seconds.

`abs' and, then, `-' is applied to TIME to ensure the used amount is always
negative."
  (interactive "P")

  (mizik--seek (- (abs (mizik--seek-parse time)))))

(defun mizik-prev ()
  "Play previous song in the current MPD playlist.

This is a somewhat silly function as it's only just calling `mpd-prev' but
binding a `lambda' to a key binding has always seemed like poor Elisp
programming so we need a function we can use for the key binding."
  (interactive)

  (mpd-prev mpd-inter-conn))
(defun mizik-next ()
  "Play next song in the current MPD playlist.

This is a somewhat silly function as it's only just calling `mpd-next' but
binding a `lambda' to a key binding has always seemed like poor Elisp
programming so we need a function we can use for the key binding."
  (interactive)

  (mpd-next mpd-inter-conn))

(defun mizik-move-up ()
  "Move the point up one line in a Mizik buffer."
  (interactive)

  ;; we want to move to the next visible line, if the buffer's
  ;; filtered, so functions like `forward-line' won't work
  (previous-line)
  ;; `beginning-of-line' works, thanks to `previous-line' keeping to visible
  ;; lines but doesn't work in cases like `mizik--filter-buffer' where we need
  ;; to move to the beginning of the next available visible line; this is
  ;; probably overkill but we'll play it safe
  ;; `beginning-of-visual-line' respects which point is visible too much and
  ;; moves to the beginning of the invisible line (if the line above the visible
  ;; one is invisible) because the newline of the beginning of the invisible
  ;; line is visible
  (move-beginning-of-line 1))
(defun mizik-move-down ()
  "Move the point down one line in a Mizik buffer."
  (interactive)

  (next-line)
  (move-beginning-of-line 1)

  (unless (get-text-property (point) 'song-data)
    (mizik-move-up)

    (message "End of buffer")))

(defvar mizik-mode-map (let ((mode-map (make-sparse-keymap)))
                         (mapc (lambda (n)
                                 (let ((num (number-to-string n)))
                                   (define-key mode-map (kbd num)
                                                        #'digit-argument)
                                   (define-key mode-map (kbd (concat "kp-" num))
                                                        #'digit-argument)))
                               (number-sequence 0 9))
                         (define-key mode-map (kbd "-")
                                              #'negative-argument)
                         (define-key mode-map (kbd "kp-subtract")
                                              #'negative-argument)

                         (define-key mode-map (kbd "p")      #'mizik-move-up)
                         (define-key mode-map (kbd "<up>")   #'mizik-move-up)
                         (define-key mode-map (kbd "n")      #'mizik-move-down)
                         (define-key mode-map (kbd "<down>") #'mizik-move-down)

                         (define-key mode-map (kbd "RET")    #'mizik-play)
                         (define-key mode-map (kbd "j")      #'mizik-play)

                         (define-key mode-map (kbd "J")      #'mizik-jump-to-playing)

                         (define-key mode-map (kbd "SPC")    #'mizik-toggle-play)

                         (define-key mode-map (kbd "r")      #'mizik-cycle-repeat)
                         (define-key mode-map (kbd "S")      #'mizik-toggle-shuffle)

                         (define-key mode-map (kbd "f")      #'mizik-seek-forward)
                         (define-key mode-map (kbd "F")      (lambda ()
                                                               (interactive)

                                                               (mizik-seek-forward  10)))
                         (define-key mode-map (kbd "M-F")    (lambda ()
                                                               (interactive)

                                                               (mizik-seek-forward  60)))
                         (define-key mode-map (kbd "M-f")    #'mizik-next)
                         (define-key mode-map (kbd "b")      #'mizik-seek-backward)
                         (define-key mode-map (kbd "B")      (lambda ()
                                                               (interactive)

                                                               (mizik-seek-backward 10)))
                         (define-key mode-map (kbd "M-B")    (lambda ()
                                                               (interactive)

                                                               (mizik-seek-backward 60)))
                         (define-key mode-map (kbd "M-b")    #'mizik-prev)

                         (define-key mode-map (kbd "s")      #'mizik-sort-by-column)

                         (define-key mode-map (kbd "M-s")    #'mizik-filter-modify)

                         (define-key mode-map (kbd "M-L")    #'mizik-load-library)
                         (define-key mode-map (kbd "M-l")    #'mizik-load-playlist)
                         (define-key mode-map (kbd "M-q")    #'mizik-load-queue)
                         (define-key mode-map (kbd "a")      #'mizik-add-to-playlist)
                         (define-key mode-map (kbd "TAB")    #'mizik-add-to-queue)

                         (define-key mode-map (kbd "q")      #'quit-window)

                         mode-map)
  "Keymap for `mizik-mode'.")
(define-derived-mode mizik-mode special-mode "Mizik"
  "Major mode for the Mizik MPD client for Emacs.")

(defun mizik--song-transpose (downp &optional playlistp)
  "Move a song's position in a Playlist up or down, depending on DOWNP.

If PLAYLISTP, move the song's position in the MPD Playlist, as well."

  (cond
   ((/= (get-text-property 0 'sorting-character header-line-format) ? )
         (message "Cannot change songs' order of sorted buffer!"))
   ((text-property-any (point-min) (point-max) 'invisible t)
         (message "Cannot change songs' order of filtered buffer!"))
   ((if (not downp) (zerop (1- (line-number-at-pos)))
                    (>= (line-number-at-pos) (1- (line-number-at-pos (point-max)))))
         (message (if (not downp) "Beginning of buffer" "End of buffer")))
   (t    (mizik--inhibit-read-only
           (let* ((posOrig (get-text-property (point) 'original-index))
                  (posMPD                                 (1- posOrig)))
             (when playlistp
               (mpd-execute-command
                 mpd-inter-conn
                 (concat (mpd-make-cmd-concat "playlistmove"
                                              (substring (buffer-name) 2 -2))
                         " "
                         (number-to-string posMPD)
                         " "
                         (number-to-string (funcall (if downp #'1+ #'1-) posMPD)))))



             (when downp (next-line))
             (transpose-lines 1)

             (previous-line)
             (put-text-property (point)         (1+ (point))
                                'original-index (if downp (1+ posOrig) posOrig))

             (previous-line)
             (put-text-property (point)         (1+ (point))
                                'original-index (if downp posOrig      (1- posOrig)))

             (when downp (next-line)))))))
(defun mizik-playlist-song-transpose-up ()
  "Move a song's position in a Playlist up; e.g. position 6 becomes 5."
  (interactive)

  (mizik--song-transpose nil t))
(defun mizik-playlist-song-transpose-down ()
  "Move a song's position in a Playlist down; e.g. position 6 becomes 7."
  (interactive)

  (mizik--song-transpose t   t))

(defun mizik--song-remove (&optional playlistp)
  "Remove the song under the current point from the buffer.

If PLAYLISTP, remove the song from the correct MPD Playlist, as well."

  (when playlistp
    (mpd-execute-command mpd-inter-conn
                         (concat (mpd-make-cmd-concat "playlistdelete"
                                                      (substring (buffer-name) 2 -2))
                         " "
                         (number-to-string (1- (get-text-property (point)
                                                                  'original-index))))))

  (mizik--inhibit-read-only
    (let ((doi (get-text-property (point) 'original-index))
          (p                                       (point)))
      (delete-line)

      (goto-char (point-min))
      (while (not (eobp))
        (when-let* ((coi (get-text-property (point) 'original-index))
                    (                                    (> coi doi)))
          (put-text-property (point) (1+ (point)) 'original-index (1- coi)))

        (forward-line))
      (goto-char p)

      (mizik--move-to-visible-line))))
(defun mizik-playlist-song-remove ()
  "Remove the song under the current point from the Playlist."
  (interactive)

  (mizik--song-remove t))

(defvar mizik-playlist-mode-map (let ((mode-map (copy-tree mizik-mode-map)))
                                  (define-key mode-map (kbd "M-p")      #'mizik-playlist-song-transpose-up)
                                  (define-key mode-map (kbd "M-<up>")   #'mizik-playlist-song-transpose-up)
                                  (define-key mode-map (kbd "M-n")      #'mizik-playlist-song-transpose-down)
                                  (define-key mode-map (kbd "M-<down>") #'mizik-playlist-song-transpose-down)

                                  (define-key mode-map (kbd "k")        #'mizik-playlist-song-remove)

                                  mode-map)
  "Keymap for `mizik-playlist-mode'.")
(define-derived-mode mizik-playlist-mode mizik-mode "Mizik Playlist"
  "Major mode for the Mizik buffers which are handling Playlists.")

(defun mizik-queue-song-transpose-up ()
  "Move a song's position in a Queue up; e.g. position 6 becomes 5."
  (interactive)

  (mizik--song-transpose nil))
(defun mizik-queue-song-transpose-down ()
  "Move a song's position in a Queue down; e.g. position 6 becomes 7."
  (interactive)

  (mizik--song-transpose t))

(defun mizik-queue-song-remove ()
  "Remove the song under the current point from the Queue."
  (interactive)

  (mizik--song-remove))

(defvar mizik-queue-mode-map (let ((mode-map (copy-tree mizik-mode-map)))
                               (define-key mode-map (kbd "M-p")      #'mizik-queue-song-transpose-up)
                               (define-key mode-map (kbd "M-<up>")   #'mizik-queue-song-transpose-up)
                               (define-key mode-map (kbd "M-n")      #'mizik-queue-song-transpose-down)
                               (define-key mode-map (kbd "M-<down>") #'mizik-queue-song-transpose-down)

                               (define-key mode-map (kbd "k")        #'mizik-queue-song-remove)

                               (define-key mode-map (kbd "A")        #'mizik-add-to-queue-advanced)

                               mode-map)
  "Keymap for `mizik-queue-mode'.")
(define-derived-mode mizik-queue-mode mizik-mode "Mizik Queue"
  "Major mode for the Mizik buffer which is handling the Queue.")

;;;###autoload
(defun mizik ()
  "Launch the Mizik MPD client."
  (interactive)

  (add-hook 'minibuffer-setup-hook        #'mizik--on-minibuffer-setup)
  (add-hook 'window-size-change-functions #'mizik--on-window-resize)
  (add-hook 'kill-buffer-hook             #'mizik--on-buffer-kill)
  (add-hook 'kill-emacs-hook              #'mizik--on-emacs-kill)
  (and (not (memq 'mizik---mode-line-status global-mode-string))
       (setq global-mode-string
             (cond
              ((consp global-mode-string)   (add-to-list 'global-mode-string
                                                         'mizik---mode-line-status
                                                         'APPEND))
              ((not global-mode-string)     (list "" 'mizik---mode-line-status))
              ((stringp global-mode-string) (list global-mode-string
                                                  'mizik---mode-line-status))))
       (unless (timerp mizik---mode-line-timer)
         (setq mizik---mode-line-timer (run-with-timer 0
                                                       mizik-mode-line-timer-interval
                                                       #'mizik--timer-handler))))

  (mizik-load-library))



(provide 'mizik)

;; Local Variables:
;; indent-tabs-mode: nil
;; End:
;;; mizik.el ends here
